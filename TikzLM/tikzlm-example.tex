\documentclass[a4paper]{article}

\usepackage{tikzlm}
\usepackage{listings}

\author{Matthieu Moy}
\title{Example of use of TikzLM}

\begin{document}

TikzLM is a simple set of \LaTeX macros to draw the basic elements in
typical SystemC/TLM (Transaction Level Modeling) picture. There's no
real documentation, but this document serves as an example and gives
hints on how to use the package. Reading the source is the best way to
know the details.

\section{Basics}

The macro \lstinline|\scmodule{(x,y)}{nodename}{Text}| creates a
module with text \lstinline|Text| displayed, and corresponding to a
TikZ node called \lstinline|nodename| :

\begin{tikzpicture}
  \scmodule{(0,0)}{cpu}{CPU};
\end{tikzpicture}

The macro \lstinline|\scin{node}{angle}{nodename}| (and
\lstinline|\scout|) allows one to add input/output ports to the
module. \lstinline|node| is the place where the port will be drawn
(typically obtained with \lstinline|cpu.south|, or
\lstinline|modulename.15|, or so), \lstinline|angle| is a rotation
angle, used when the port isn't turned towards the right, and
\lstinline|nodename| is the name of the port created (it corresponds
to the point where the output wires will be connected).


\begin{tikzpicture}
  \scmodule{(0,0)}{cpu}{CPU};
  \scin{cpu.east}{0}{cpu_irq};

  \scmodule{(4,1)}{ITC}{ITC};
  \scout{ITC.west}{180}{itc_irq};
\end{tikzpicture}

Similarly, \lstinline|\tsocket| (resp. \lstinline|\isocket|) allow drawing
TLM target (resp. initiator) sockets (different just by the way they
are drawn).

To connect ports and sockets, one can use basically
\lstinline|\draw(...) -- (...);| like this:


\begin{tikzpicture}
  \scmodule{(0,0)}{cpu}{CPU};
  \scin{cpu.east}{0}{cpu_irq};

  \scmodule{(4,1)}{ITC}{ITC};
  \scout{ITC.west}{180}{itc_irq};

  \draw(itc_irq) -- (cpu_irq);
\end{tikzpicture}

Alternatively, \lstinline|\hbind| and \lstinline|\vbind| avoid
non-horizontal/vertical lines:

\begin{tikzpicture}
  \scmodule{(0,0)}{cpu}{CPU};
  \scin{cpu.east}{0}{cpu_irq};

  \scmodule{(4,1)}{ITC}{ITC};
  \scout{ITC.west}{180}{itc_irq};

  \hbind{itc_irq}{cpu_irq};
\end{tikzpicture}

\begin{tikzpicture}
  \scmodule{(0,0)}{cpu}{CPU};
  \scin{cpu.north}{90}{cpu_irq};

  \scmodule{(1,3)}{ITC}{ITC};
  \scout{ITC.south}{-90}{itc_irq};

  \vbind{itc_irq}{cpu_irq};
\end{tikzpicture}

\section{Example of platform with a bus}

\lstinline|\tlmbus| is a simple wrapper around \lstinline|\scmodule|
giving a long, thin shape to look like a bus.

One can either connect the components directly to the bus with
\lstinline|\hbindbus{socket}{point}| (\lstinline|point| is either
\lstinline|Bus.south| or \lstinline|Bus.north| depending on whether
the component to connect is above or below the bus):

\begin{tikzpicture}
% Can help for debugging
%  \draw[step=1cm,gray!30!white,very thin] (0,0) grid (10,10);

  \scmodule{(2,6)}{CPU}{CPU};
  \isocket{CPU.south}{-90}{cpu_socket};
  \scin{CPU.east}{0}{cpu_irq};

  \scmodule{(8,6)}{ITC}{ITC};
  \tsocket{ITC.south}{90}{itc_socket};
  \scout{ITC.west}{180}{itc_irq};

  \scmodule{(5,1)}{RAM}{RAM};
  \tsocket{RAM.north}{-90}{ram_socket};

  \draw (itc_irq) -- (cpu_irq);

  \tlmbus{(5,3.5)}{Bus}{Bus};

  \hbindbus{cpu_socket}{Bus.north};
  \hbindbus{itc_socket}{Bus.north};
  \hbindbus{ram_socket}{Bus.south};

\end{tikzpicture}

... or show explicitly the sockets of the bus:

\begin{tikzpicture}

  \scmodule{(2,6)}{CPU}{CPU};
  \isocket{CPU.south}{-90}{cpu_socket};
  \scin{CPU.east}{0}{cpu_irq};

  \scmodule{(8,6)}{ITC}{ITC};
  \tsocket{ITC.south}{90}{itc_socket};
  \scout{ITC.west}{180}{itc_irq};

  \scmodule{(5,1)}{RAM}{RAM};
  \tsocket{RAM.north}{-90}{ram_socket};

  \draw (itc_irq) -- (cpu_irq);

  \tlmbus{(5,3.5)}{Bus}{Bus};
  \tsocket{Bus.north}{-90}{bus_target};
  \isocket{Bus.south}{-90}{bus_initiator};

  \vbind{cpu_socket}{bus_target};
  \vbind{itc_socket}{bus_target};
  \vbind{bus_initiator}{ram_socket};
\end{tikzpicture}

\section{Bigger example}

\begin{tikzpicture}
  \tlmbus[minimum width=15cm,minimum height=1cm]{(8,3.5)}{Bus}{Bus};

  \scmodule{(2,6)}{CPU}{CPU};
  \isocket{CPU.south}{-90}{cpu_socket};
  \hbindbus{cpu_socket}{Bus.north};
  \scin{CPU.east}{0}{cpu_irq};

  \scmodule{(6,6)}{ITC}{ITC};
% Y est dans la vraie plateforme, mais pas dans notre version TLM.
%  \tsocket{ITC.south}{90}{itc_socket};
%  \hbindbus{itc_socket}{Bus.north};
  \scout{ITC.west}{180}{itc};
  \scin{ITC.15}{0}{itc_in0};
  \scin{ITC.-15}{0}{itc_in1};

  \draw (itc) -- (cpu_irq);

  \scmodule{(4,1)}{data_RAM}{Data RAM};
  \tsocket{data_RAM.north}{-90}{ram_socket};
  \hbindbus{ram_socket}{Bus.south};

  \scmodule{(8,1)}{inst_RAM}{Instruction RAM};
  \tsocket{inst_RAM.north}{-90}{inst_ram_socket};
  \hbindbus{inst_ram_socket}{Bus.south};

  \scmodule{(12,1)}{GPIO}{GPIO};
  \tsocket{GPIO.north}{-90}{gpio_socket};
  \hbindbus{gpio_socket}{Bus.south};

  \scmodule{(10,6)}{VGA}{VGA};
  \tsocket{VGA.230}{90}{vga_target};
  \isocket{VGA.310}{-90}{vga_initiator};
  \hbindbus{vga_target}{Bus.north};
  \hbindbus{vga_initiator}{Bus.north};
  \scout{VGA.west}{180}{vga_irq};

  \hbind{vga_irq}{itc_in1};

  \scmodule{(14,6)}{timer}{Timer};
  \tsocket{timer.south}{90}{timer_socket};
  \hbindbus{timer_socket}{Bus.north};
  \scout{timer.west}{180}{timer_irq};

  \draw (VGA.north) +(0,.5) coordinate (abovevga);
  \draw (abovevga -| VGA.west) coordinate (abovevgaw);
  \draw (abovevga -| VGA.east) coordinate (abovevgae);

  \hbind{timer_irq}{abovevgae};
  \draw(abovevgae) -- (abovevgaw);
  \hbind{abovevgaw}{itc_in0};
\end{tikzpicture}

\section{Draw a memory map on the side}

\begin{tikzpicture}
  % Can help for debugging
  % \draw[step=1cm,gray!30!white,very thin] (0,0) grid (10,5);

  \tlmbus{(4.5,3.5)}{Bus}{Bus};

  \scmodule[minimum width=7cm, minimum height=2.5cm]{(1,7)}{CPU}{};
  \node (cpu_title) at (CPU.north west) [anchor=north west] {CPU};
  \isocket{CPU.south}{-90}{cpu_socket};
  \path (Bus.north-|CPU.south) ++ (1,0) coordinate (bustsocket);
  \tsocket{bustsocket}{-90}{bus_tsocket};
  \vbind{cpu_socket_out}{bus_tsocket_in};

  \scmodule[minimum width=7cm, minimum height=2.5cm]{(7.5,0)}{RAM}{};
  \node (cpu_title) at (RAM.north west) [anchor=north west] {RAM};
  \tsocket{RAM.north}{-90}{ram_socket};
  \path (Bus.south-|RAM.north) ++(-1, 0) coordinate (busisocket);
  \isocket{busisocket}{-90}{bus_isocket};
  \vbind{ram_socket_in}{bus_isocket_out};

  % Show that other targets could have been there
  \draw (2.5,2) node (target1) {T1};
  \draw (4.5,2) node (target2) {T2};

  \isocket{target1 |- Bus.south}{-90}{bus_isocket1};
  \isocket{target2 |- Bus.south}{-90}{bus_isocket2};

  \vbind{target1.north}{bus_isocket1};
  \vbind{target2.north}{bus_isocket2};

  {
    \tikzstyle{mmap_line}=[draw=white!50!black]
    \tikzstyle{mmap_text}=[color=white!50!black]
    \tikzstyle{mmap_fill}=[fill=white!97!black]

    \memorymap{8,4}{3}{4};

    \map{1}{1.5}{0x0000}{0x1000}{T1};
    \map{2}{2.5}{0x2000}{0x3000}{T2};
    \map{3.5}{4}{0x5000}{0x6000}{RAM};
  }

  {
    \codebox{CPU}{\footnotesize \dots{}\\socket.write(addr,data);\\ \dots{}};
    \codebox{RAM}{status write(addr,data) \{\\
      ~~~~mem[addr] = data;\\
      \}};
  }

  {
    \draw[dashed, very thick, draw=red!50!black] (cpu_socket_in)
    .. controls (cpu_socket |- Bus.south) and (Bus.north -| ram_socket) ..
    (ram_socket_out);
  }
\end{tikzpicture}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
